#! /bin/bash
# this script tries to recreat old nick-accounts

oldAccount="cccs,icloud,netz39,shack,+,oeffi,remote,github,discord,fritz,docker,vimeo,soundcloud,edx,server,imgur,images,stackoverflow,wordpress,hacker,bitbucket,choascongress,news,instatest,craiglist,mactechnews,chaoscongress,test.fb,fb.test,nda,wiki,choas,social,test,c3love,c3love.wiki,game,chat,volunteer,mail,wohnung,congstar,airbnb,movies,pizza,linux,blackmosquito,sÃlx,games,redhat,ebay-kleinanzeigen,deezer,sushifreunde,okf,openstreetmap,archlinux,cycling,solidrinks,jugend.presse,copay,die.partei,pottermore,steam,gitlab,pep.coop,hacker.coop,z3dd3l,junge.welt,wolke,concerts,tux,wp,help,hack2thefuture,35c3,trello,apple.pay,turris.backup,turris,linuxtage,bahn,motherless,vote4eu,money,cinnema,fragdenstaat,humnlr,humblr,liberapay,hackerforum,ismygirl,games2gether,machteburch.social,seashepherd,freifunk,admin.hackerforum,verein.hackercoop,shopping,anker,impericon,imp,impo,test1,demo,vebit,matomo,bandcamp,patreon,reddit,riot,magic,vebit.git,dezentrale,winehq,abgeordnetenwatch,paypal,cbase,minecraft,pycamp,riseup,codimd,liz,lpme,crossover,aws,ifttt,logitech,keybase,hacker.news,imgflip,studio-link,c-base,ard,t3n" 

Field_Separator=$IFS

# set comma as internal field separator for the string list
IFS=,
for val in $oldAccount;
do
 echo "### recreating $val ###"

 localpart=$val

 uberspace mail user del $localpart
 
 rm ~/.qmail-$localpart
 
 uberspace mail user forward set $localpart therojam
done
IFS=$Field_Separator
