#! /bin/bash

localpart=$@

#echo "Which new localpart would you like to use?"
#
#read localpart

uberspace mail user add $localpart

cp ~/.qmail-therojam ~/.qmail-$localpart

vchattr --enabled 0 $localpart

# DestServer="cache-1-11,cache-1-12" 
# Field_Separator=$IFS
#
# # set comma as internal field separator for the string list
# IFS=,
# for val in $DestServer;
# do
#  echo "### transfer to $val ###"
#  scp $@-ssl $val:~/.
#  scp $@ $val:~/.
# done
# IFS=$Field_Separator
