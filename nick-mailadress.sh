#! /bin/bash

# read the localpart of the new mailadress from first argument ($1) 
localpart=$1

# echo "Which mailaddress for your nick-domain would you like to add as forwarding?"
# 
# read localpart

# create new mailadress (virtualuser) w/ $localpart and 
# ask for password for this new mailadress
vadduser nick-$localpart

# disable mailadress
vchattr --enabled 0 nick-$localpart

# create forwarding for this mailadress
cp ~/.qmail-nick-icloud ~/.qmail-nick-$localpart
