#! /bin/bash
echo "Which mailaddress for your normal-domain would you like to add as forwarding?"

read nick

vadduser $nick

cp ~/.qmail-alex ~/.qmail-$nick

vchattr --enabled 0 $nick
